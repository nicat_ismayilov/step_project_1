$('.our-services-tabs-content li:not(:first)').hide();
$('.our-services-tabs').click((e) => {
      $(".active").removeClass("active");
      $(e.target).addClass("active");
      if ($(e.target).text() == 'online marketing') {
            // $('.our-services').css({'height' : '410px'});
      }
      $('#tab_content li').hide();
      $(`#tab_content li[data-content=${e.target.dataset.content}]`).show()
})


$('.amazing-work-content .amazing-work-tab-content:not(:first)').hide();
$('.load-more-btn').show();
$('.amazing-work-tab-bar').click((e) => {
      $(".active-tab").removeClass("active-tab");
      $(e.target).addClass("active-tab");
      if($(e.target).text() == 'all') {
            $('button.load-more-btn').show();
            $('.our-amazing-work').css({ 'height': '800px' });
            $('.amazing-work-section-container').css({ 'height': '700px' });
      }
      else {
            $('.load-more-btn').hide();
            $('.our-amazing-work').css({'height' : '685px'});
            $('.amazing-work-section-container').css({ 'height': '585px' });
      }
      $('#tab_content_amazing .amazing-work-tab-content').hide();
      $(`#tab_content_amazing .amazing-work-tab-content[data-content=${e.target.dataset.content}]`).show()
})


// Carousel Part ------------------------------------

const carousel_slide = document.querySelector('.customer-feedback-area');
const carousel_divs = document.querySelectorAll('.customer-feedback-content');
const carousel_small = document.querySelectorAll('.customer-img-outher-shadow-small');
const small_borders = document.querySelectorAll('.customer-img-solid-shadow-small');

const prev_btn = document.querySelector('#prevBtn');
const next_btn = document.querySelector('#nextBtn');

let counter = 1;

const size = carousel_divs[1].clientWidth;

carousel_slide.style.transform = 'translateX(' + (-size * counter) + 'px)';
carousel_small[counter - 1].style.transform = 'translateY(-3px)';
small_borders[counter - 1].style.backgroundColor = '#18cfab'

next_btn.addEventListener('click', () => {
      if (counter >= (carousel_divs.length - 1)) return;
      carousel_slide.style.transition = 'transform 0.4s ease-in-out';
      carousel_small[counter - 1].style.transform = 'translateY(3px)';
      small_borders[counter - 1].style.backgroundColor = 'rgba(31, 217, 181, 0.2)'
      counter++;
      carousel_slide.style.transform = 'translateX(' + (-size  * counter) + 'px)';
      carousel_small[counter - 1].style.transform = 'translateY(-3px)';
      small_borders[counter - 1].style.backgroundColor = '#18cfab'
});

prev_btn.addEventListener('click', () => {
      if (counter <= 0) return;
      carousel_slide.style.transition = 'transform 0.4s ease-in-out';
      carousel_small[counter - 1].style.transform = 'translateY(3px)';
      small_borders[counter - 1].style.backgroundColor = 'rgba(31, 217, 181, 0.2)'
      counter--;
      carousel_slide.style.transform = 'translateX(' + (-size * counter) + 'px)';
      carousel_small[counter - 1].style.transform = 'translateY(-3px)';
      small_borders[counter - 1].style.backgroundColor = '#18cfab'
});

carousel_slide.addEventListener('transitionend', () => {
      if (carousel_divs[counter].id === 'feedback-last') {
            carousel_slide.style.transition = 'none';
            counter = carousel_divs.length - 2;
            carousel_small[counter - 1].style.transform = 'translateY(-3px)';
            small_borders[counter - 1].style.backgroundColor = '#18cfab'
            carousel_slide.style.transform = 'translateX(' + (-size * counter) + 'px)';
      }

      if (carousel_divs[counter].id === 'feedback-first') {
            carousel_slide.style.transition = 'none';
            counter = carousel_divs.length - counter;
            carousel_small[counter - 1].style.transform = 'translateY(-3px)';
            small_borders[counter - 1].style.backgroundColor = '#18cfab'
            carousel_slide.style.transform = 'translateX(' + (-size * counter) + 'px)';
      }
});